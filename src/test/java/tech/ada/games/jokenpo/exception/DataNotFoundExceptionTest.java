package tech.ada.games.jokenpo.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DataNotFoundExceptionTest {

    @Test
    public void testCoverageForDataNotFoundException() {
        String message = "data not found";

        DataNotFoundException exception = new DataNotFoundException(message);

        assertEquals("data not found", exception.getMessage());
    }

}