package tech.ada.games.jokenpo.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DataConflictExceptionTest {

    @Test
    public void testCoverageForDataConflictException() {
        String message = "data conflict";

        DataConflictException exception = new DataConflictException(message);

        assertEquals("data conflict", exception.getMessage());
    }

}