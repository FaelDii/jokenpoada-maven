package tech.ada.games.jokenpo.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BadRequestExceptionTest {
    
    @Test
    public void testCoverageForBadRequestException() {
        String message = "bad request";
        
        BadRequestException exception = new BadRequestException(message);
        
        assertEquals("bad request", exception.getMessage());
    }

}