package tech.ada.games.jokenpo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;

import tech.ada.games.jokenpo.dto.LoginDto;
import tech.ada.games.jokenpo.response.AuthResponse;
import tech.ada.games.jokenpo.security.JwtTokenProvider;

@ExtendWith(MockitoExtension.class)
public class AuthServiceTest {
	
	@InjectMocks
	private AuthService authService;
	
	@Mock
	AuthenticationManager authenticationManager;
	
	@Mock
	JwtTokenProvider tokenProvider;
	
	private LoginDto loginDto;
	
	private final String token = "eyJhbGciOiJIUzI1NiJ9.eyJtZXNzYWdlIjoidGVzdCBvay4ifQ.NRSKaQ3m_Y1V5X-nlg4wwucZDy0MC39Izr7VPcU_4Cg";
		
	@BeforeEach
	void setUp() {
		
		
		this.loginDto = new LoginDto();
		
	}
	
	@Test
	void testLogin_Success() {
		
		this.loginDto.setUsername("david1");
		this.loginDto.setPassword("1234@A");

		Authentication authentication= mock(Authentication.class);
		authentication.setAuthenticated(true);

	    when(authenticationManager.authenticate(any())).thenReturn(authentication);
	    when(tokenProvider.generateToken(any())).thenReturn(this.token);
		
	    AuthResponse authResponse = authService.login(loginDto);

		verify(tokenProvider, times(1)).generateToken(any());
		
		assertNotNull(authResponse);
		assertNotNull(authResponse.getAccessToken());
		assertEquals(this.token, authResponse.getAccessToken());
	}
	
	@Test
	void testLogin_Fail() {
		
		this.loginDto.setUsername("david1");
		this.loginDto.setPassword("1234@A");

		Authentication authentication= mock(Authentication.class);
		authentication.setAuthenticated(false);

	    when(authenticationManager.authenticate(any())).thenReturn(authentication);
	    when(tokenProvider.generateToken(any())).thenReturn(null);
		
	    AuthResponse authResponse = authService.login(loginDto);

		verify(tokenProvider, times(1)).generateToken(any());
		
		assertNotNull(authResponse);
		assertNull(authResponse.getAccessToken());
	}
	

}
