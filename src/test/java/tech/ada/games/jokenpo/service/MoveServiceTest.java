package tech.ada.games.jokenpo.service;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import tech.ada.games.jokenpo.repository.GameRepository;
import tech.ada.games.jokenpo.repository.MoveRepository;

import static org.junit.jupiter.api.Assertions.*;

class MoveServiceTest {

    @InjectMocks
    private MoveService moveService;

    @Mock
    private MoveRepository moveRepository;

    @Test
    void createMove() {
    }

    @Test
    void findMoves() {
    }

    @Test
    void findByMove() {
    }
}