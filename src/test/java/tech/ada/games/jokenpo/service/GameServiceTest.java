package tech.ada.games.jokenpo.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import tech.ada.games.jokenpo.dto.GameDto;
import tech.ada.games.jokenpo.dto.GameMoveDto;
import tech.ada.games.jokenpo.dto.ResultDto;
import tech.ada.games.jokenpo.exception.BadRequestException;
import tech.ada.games.jokenpo.exception.DataConflictException;
import tech.ada.games.jokenpo.exception.DataNotFoundException;
import tech.ada.games.jokenpo.model.Game;
import tech.ada.games.jokenpo.model.Move;
import tech.ada.games.jokenpo.model.Player;
import tech.ada.games.jokenpo.model.PlayerMove;
import tech.ada.games.jokenpo.repository.GameRepository;
import tech.ada.games.jokenpo.repository.PlayerMoveRepository;
import tech.ada.games.jokenpo.repository.MoveRepository;
import tech.ada.games.jokenpo.repository.PlayerRepository;
import tech.ada.games.jokenpo.security.SecurityUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GameServiceTest {

    @InjectMocks
    private GameService gameService;

    @Mock
    private GameRepository gameRepository;

    @Mock
    private PlayerMoveRepository playerMoveRepository;

    @Mock
    private MoveRepository moveRepository;

    @Mock
    private PlayerRepository playerRepository;

    private Player currentPlayer;
    private GameDto gameDto;
    private Game game;
    private PlayerMove playerMove;
    private MockedStatic<SecurityUtils> mockedSecurityUtils;

    @BeforeEach
    void setUp() {
        currentPlayer = new Player();
        currentPlayer.setId(1L);
        currentPlayer.setUsername("usuario");
        currentPlayer.setName("Test Player");

        gameDto = new GameDto();
        game = new Game();
        playerMove = new PlayerMove();

        mockedSecurityUtils = Mockito.mockStatic(SecurityUtils.class);
    }

    @AfterEach
    void tearDown() {
        if (mockedSecurityUtils != null) {
            mockedSecurityUtils.close();
        }
    }

    @Test
    void testNewGame_Success() throws BadRequestException, DataNotFoundException {
        gameDto.setPlayers(Arrays.asList(1L, 2L));
        mockedSecurityUtils.when(SecurityUtils::getCurrentUserLogin).thenReturn("usuario");
        when(playerRepository.findByUsername(any())).thenReturn(java.util.Optional.of(currentPlayer));
        when(playerRepository.findById(any())).thenReturn(java.util.Optional.of(currentPlayer));

        gameService.newGame(gameDto);

        verify(gameRepository, times(1)).save(any());
        verify(playerMoveRepository, times(2)).save(any());
    }

    @Test
    void testNewGame_LessThanTwoPlayers() {
        gameDto.setPlayers(Arrays.asList(1L));
        mockedSecurityUtils.when(SecurityUtils::getCurrentUserLogin).thenReturn("usuario");
        when(playerRepository.findByUsername(any())).thenReturn(java.util.Optional.of(currentPlayer));

        Exception exception = assertThrows(BadRequestException.class, () -> gameService.newGame(gameDto));
        assertEquals("O jogo possui menos que dois jogadores!", exception.getMessage());
    }

    @Test
    void testNewGame_PlayerNotRegistered() {
        gameDto.setPlayers(Arrays.asList(1L, 2L));
        mockedSecurityUtils.when(SecurityUtils::getCurrentUserLogin).thenReturn("usuario");

        Exception exception = assertThrows(DataNotFoundException.class, () -> gameService.newGame(gameDto));
        assertEquals("O jogador não está cadastrado!", exception.getMessage());
    }

    @Test
    void testInsertPlayerMove_PlayerNotFound() {
        GameMoveDto gameMove = new GameMoveDto(1L, 1L);
        mockedSecurityUtils.when(SecurityUtils::getCurrentUserLogin).thenReturn("usuario");
        when(playerRepository.findByUsername(any())).thenReturn(Optional.empty());

        Exception exception = assertThrows(DataNotFoundException.class, () -> gameService.insertPlayerMove(gameMove));
        assertEquals("O jogador não está cadastrado!", exception.getMessage());
    }

    @Test
    void testInsertPlayerMove_GameNotFound() {
        GameMoveDto gameMove = new GameMoveDto(1L, 1L);
        mockedSecurityUtils.when(SecurityUtils::getCurrentUserLogin).thenReturn("usuario");
        when(playerRepository.findByUsername(any())).thenReturn(Optional.of(currentPlayer));
        when(gameRepository.findById(any())).thenReturn(Optional.empty());

        Exception exception = assertThrows(DataNotFoundException.class, () -> gameService.insertPlayerMove(gameMove));
        assertEquals("Jogo não cadastrado!", exception.getMessage());
    }

    @Test
    void testInsertPlayerMove_MoveNotFound() {
        GameMoveDto gameMove = new GameMoveDto(1L, 1L);
        game.setFinished(false);
        mockedSecurityUtils.when(SecurityUtils::getCurrentUserLogin).thenReturn("usuario");
        when(playerRepository.findByUsername(any())).thenReturn(Optional.of(currentPlayer));
        when(gameRepository.findById(any())).thenReturn(Optional.of(game));
        when(moveRepository.findById(any())).thenReturn(Optional.empty());

        Exception exception = assertThrows(DataNotFoundException.class, () -> gameService.insertPlayerMove(gameMove));
        assertEquals("Jogada não cadastrada", exception.getMessage());
    }

    @Test
    void testInsertPlayerMove_GameAlreadyFinished() {
        GameMoveDto gameMove = new GameMoveDto(1L, 1L);
        game.setFinished(true);
        mockedSecurityUtils.when(SecurityUtils::getCurrentUserLogin).thenReturn("usuario");
        when(playerRepository.findByUsername(any())).thenReturn(Optional.of(currentPlayer));
        when(gameRepository.findById(any())).thenReturn(Optional.of(game));

        Exception exception = assertThrows(BadRequestException.class, () -> gameService.insertPlayerMove(gameMove));
        assertEquals("O jogo já foi finalizado!", exception.getMessage());
    }

    @Test
    void testInsertPlayerMove_PlayerNotRegisteredInGame() {
        Long gameId = 1l;
        Long moveId = 1l;

        game.setId(gameId);
        game.setFinished(false);

        GameMoveDto gameMoveDto = new GameMoveDto();
        gameMoveDto.setGameId(gameId);
        gameMoveDto.setMoveId(moveId);

        mockedSecurityUtils.when(SecurityUtils::getCurrentUserLogin).thenReturn("usuario");
        when(gameRepository.findById(gameId)).thenReturn(Optional.of(game));
        when(playerRepository.findByUsername(currentPlayer.getUsername())).thenReturn(Optional.of(currentPlayer));
        when(playerMoveRepository.findByUnfinishedGameIdAndPlayer(gameId, currentPlayer.getId())).thenReturn(Optional.empty());
        when(moveRepository.findById(moveId)).thenReturn(Optional.of(new Move()));

        DataNotFoundException exception = assertThrows(DataNotFoundException.class, () -> gameService.insertPlayerMove(gameMoveDto));
        assertEquals("Jogador não está cadastrado no jogo!", exception.getMessage());
    }

    @Test
    void testInsertPlayerMove_AllMovesMadeAndGenerateFinalResult() throws DataNotFoundException, DataConflictException, BadRequestException {
        GameMoveDto gameMove = new GameMoveDto(1L, 1L);

        playerMove.setMove(null);
        playerMove.setPlayer(currentPlayer);
        game.setFinished(false);

        mockedSecurityUtils.when(SecurityUtils::getCurrentUserLogin).thenReturn("usuario");
        when(playerRepository.findByUsername(any())).thenReturn(Optional.of(currentPlayer));
        when(gameRepository.findById(any())).thenReturn(Optional.of(game));
        when(moveRepository.findById(any())).thenReturn(Optional.of(new Move()));
        when(playerMoveRepository.findByUnfinishedGameIdAndPlayer(any(), any())).thenReturn(Optional.of(playerMove));
        when(playerMoveRepository.countMovesPlayedByUnfinishedGame(any())).thenReturn(2L);
        when(playerMoveRepository.countByUnfinishedGameId(any())).thenReturn(2L);


        when(playerMoveRepository.existsSpockByUnfinishedGameId(any())).thenReturn(true);
        when(playerMoveRepository.existsTesouraByUnfinishedGameId(any())).thenReturn(true);
        when(playerMoveRepository.existsPapelByUnfinishedGameId(any())).thenReturn(false);
        when(playerMoveRepository.existsPedraByUnfinishedGameId(any())).thenReturn(false);
        when(playerMoveRepository.existsLagartoByUnfinishedGameId(any())).thenReturn(false);
        when(playerMoveRepository.findByUnfinishedGameId(any(), any())).thenReturn(Collections.singletonList(playerMove));

        ResultDto result;
        result = gameService.insertPlayerMove(gameMove);
        assertNotNull(result);
        assertEquals("Vencedor: " + currentPlayer.getName(), result.getMessage());
    }

    @Test
    void testInsertPlayerMove_PlayerAlreadyMadeMove() {
        GameMoveDto gameMove = new GameMoveDto(1L, 1L);
        playerMove.setMove(new Move());
        game.setFinished(false);

        mockedSecurityUtils.when(SecurityUtils::getCurrentUserLogin).thenReturn("usuario");
        when(playerRepository.findByUsername(any())).thenReturn(Optional.of(currentPlayer));
        when(gameRepository.findById(any())).thenReturn(Optional.of(game));
        when(moveRepository.findById(any())).thenReturn(Optional.of(new Move()));
        when(playerMoveRepository.findByUnfinishedGameIdAndPlayer(any(), any())).thenReturn(Optional.of(playerMove));

        Exception exception = assertThrows(DataConflictException.class, () -> gameService.insertPlayerMove(gameMove));
        assertEquals("Jogador já realizou a sua jogada!", exception.getMessage());
    }
}
