package tech.ada.games.jokenpo.steps.player;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;
import tech.ada.games.jokenpo.controller.PlayerController;
import tech.ada.games.jokenpo.dto.PlayerDto;
import tech.ada.games.jokenpo.model.Player;
import tech.ada.games.jokenpo.service.PlayerService;

import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class AddPlayerWithValidDataStep {


    private static final String API_URL = "/api/v1/jokenpo/player/create";

    private final PlayerService playerService = Mockito.mock(PlayerService.class);

    private final MockMvc mockMvc;

    private ResultActions resultActions;

    public AddPlayerWithValidDataStep() {
        PlayerController playerController = new PlayerController(playerService);
        this.mockMvc = MockMvcBuilders.standaloneSetup(playerController).setViewResolvers((s, locale) -> new MappingJackson2JsonView()).build();
    }

    @Given("que um usuario quer jogar e ele nao esta cadastrado")
    public void que_um_usuario_quer_jogar_e_ele_nao_esta_cadastrado() {
    }

    @When("clica em criar usuario fornecendo name, username e password")
    public void clica_em_criar_usuario_fornecendo_name_username_e_password() throws Exception {
        Player player = new Player();
        player.setUsername("line.cruz");
        player.setName("Aline");
        player.setPassword("aline2345");
        PlayerDto playerDto = new PlayerDto(player);
        doNothing().when(playerService).createPlayer(playerDto);

        String jsonPlayer = "{\"username\": \"line.cruz\",\"password\": \"aline2345\"}";

        resultActions = mockMvc.perform(post(API_URL).content(jsonPlayer).contentType(MediaType.APPLICATION_JSON));
    }

    @Then("seu usuario e criado com os dados passados")
    public void seu_usuario_e_criado_com_os_dados_passados() throws Exception {
        resultActions.andExpect(MockMvcResultMatchers.status().isCreated());
    }

}
