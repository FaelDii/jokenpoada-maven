package tech.ada.games.jokenpo.steps.player;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;
import tech.ada.games.jokenpo.controller.PlayerController;
import tech.ada.games.jokenpo.dto.PlayerDto;
import tech.ada.games.jokenpo.model.Player;
import tech.ada.games.jokenpo.service.PlayerService;

import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class AddPlayerWithoutData {

    private static final String API_URL = "/api/v1/jokenpo/player/create";

    private final PlayerService playerService = Mockito.mock(PlayerService.class);

    private final MockMvc mockMvc;

    private ResultActions resultActions;

    public AddPlayerWithoutData() {
        PlayerController playerController = new PlayerController(playerService);
        this.mockMvc = MockMvcBuilders.standaloneSetup(playerController)
                .setViewResolvers((s, locale) -> new MappingJackson2JsonView())
                .build();
    }

    @Given("que um usuario quer jogar e nao esta cadastrado")
    public void que_um_usuario_quer_jogar_e_nao_esta_cadastrado() {
    }

    @When("clica em criar usuario e nao fornece nenhum dado")
    public void clica_em_criar_usuario_e_nao_fornece_nenhum_dado() throws Exception {
        Player player = new Player();
        PlayerDto playerDto = new PlayerDto(player);
        doNothing().when(playerService).createPlayer(playerDto);

        resultActions = mockMvc.perform(post(API_URL)
                .contentType(MediaType.APPLICATION_JSON));
    }

    @Then("retorna erro e nao cria o usuario")
    public void retorna_erro_e_nao_cria_o_usuario() throws Exception {
        resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
