package tech.ada.games.jokenpo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
@TestPropertySource(locations="classpath:application-tests.properties")
class JokenpoadaApplicationTests {

	@Test
	void contextLoads() {
	}

}
