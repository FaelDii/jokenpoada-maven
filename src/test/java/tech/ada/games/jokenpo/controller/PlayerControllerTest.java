package tech.ada.games.jokenpo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import tech.ada.games.jokenpo.dto.LoginDto;
import tech.ada.games.jokenpo.dto.PlayerDto;
import tech.ada.games.jokenpo.response.AuthResponse;
import tech.ada.games.jokenpo.service.AuthService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestPropertySource(locations="classpath:application-tests.properties")
public class PlayerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AuthService service;

    @Test
    @Order(1)
    public void shouldCreatePlayer() throws Exception {
        PlayerDto playerDto = new PlayerDto();
        playerDto.setName("Rafael Dias");
        playerDto.setUsername("faeldi");
        playerDto.setPassword("!2#4%6");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(playerDto);
        this.mockMvc.perform(
                post("/api/v1/jokenpo/player/create")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    @Order(1)
    public void shouldFindPlayers()  throws Exception  {

        LoginDto loginDto = new LoginDto();
        loginDto.setUsername("faeldi");
        loginDto.setPassword("!2#4%6");
        AuthResponse authResponse = service.login(loginDto);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(loginDto);
        this.mockMvc.perform(
                        post("/api/v1/jokenpo/login")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .content(requestJson))
                .andDo(print())
                .andExpect(status().isOk());

        this.mockMvc.perform(
                        get("/api/v1/jokenpo/player")
                                .header("Authorization",authResponse.getTokenType()+" "+authResponse.getAccessToken()))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Order(2)
    public void shouldNotCreatePlayer() throws Exception {
        PlayerDto playerDto = new PlayerDto();
        playerDto.setName("Rafael Dias");
        playerDto.setUsername("faeldi");
        playerDto.setPassword("!2#4%6");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(playerDto);

        //Tentando criar mesmo player
        this.mockMvc.perform(
                        post("/api/v1/jokenpo/player/create")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .content(requestJson))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    @Order(3)
    public void shouldFindPlayer()  throws Exception  {

        LoginDto loginDto = new LoginDto();
        loginDto.setUsername("faeldi");
        loginDto.setPassword("!2#4%6");
        AuthResponse authResponse = service.login(loginDto);

        this.mockMvc.perform(
                        get("/api/v1/jokenpo/player/{player}","faeldi")
                                .header("Authorization",authResponse.getTokenType()+" "+authResponse.getAccessToken()))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Order(4)
    public void shouldDeletePlayer()  throws Exception  {

        LoginDto loginDto = new LoginDto();
        loginDto.setUsername("faeldi");
        loginDto.setPassword("!2#4%6");
        AuthResponse authResponse = service.login(loginDto);

        this.mockMvc.perform(
                        delete("/api/v1/jokenpo/player/{playerId}",1L)
                                .header("Authorization",authResponse.getTokenType()+" "+authResponse.getAccessToken()))
                .andDo(print())
                .andExpect(status().isNoContent());
    }




}