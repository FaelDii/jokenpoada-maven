package tech.ada.games.jokenpo.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import tech.ada.games.jokenpo.dto.*;
import tech.ada.games.jokenpo.model.Game;
import tech.ada.games.jokenpo.model.Player;
import tech.ada.games.jokenpo.model.PlayerMove;
import tech.ada.games.jokenpo.response.AuthResponse;
import tech.ada.games.jokenpo.service.AuthService;
import tech.ada.games.jokenpo.service.GameService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.testng.AssertJUnit.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestPropertySource(locations = "classpath:application-tests.properties")
public class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AuthService authService;

    @MockBean
    private GameService gameService;

    @Autowired
    private ObjectMapper objectMapper;

    private AuthResponse authResponse;

    @Test
    @Order(1)
    public void shouldCreatePlayer() throws Exception {
        LoginDto loginDto = createLoginDto();
        PlayerDto playerDto = new PlayerDto();
        playerDto.setName("Teste name");
        playerDto.setUsername(loginDto.getUsername());
        playerDto.setPassword(loginDto.getPassword());
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(playerDto);
        this.mockMvc.perform(post("/api/v1/jokenpo/player/create").contentType(MediaType.APPLICATION_JSON_VALUE).content(requestJson)).andDo(print()).andExpect(status().isCreated());
    }

    @Test
    @Order(2)
    public void shouldFindPlayers() throws Exception {
        LoginDto loginDto = createLoginDto();
        authResponse = authService.login(loginDto);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(loginDto);
        this.mockMvc.perform(post("/api/v1/jokenpo/login").contentType(MediaType.APPLICATION_JSON_VALUE).content(requestJson)).andDo(print()).andExpect(status().isOk());

        this.mockMvc.perform(get("/api/v1/jokenpo/player").header("Authorization", authResponse.getTokenType() + " " + authResponse.getAccessToken())).andDo(print()).andExpect(status().isOk());
    }

    @Test
    @Order(3)
    public void testFindGameById() throws Exception {
        authResponse = authService.login(createLoginDto());

        Game game = createGame(1L, "player1", "player2", true);

        when(gameService.findGameById(ArgumentMatchers.any())).thenReturn(game);

        mockMvc.perform(get("/api/v1/jokenpo/game/1").header("Authorization", authResponse.getTokenType() + " " + authResponse.getAccessToken())).andExpect(status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L)).andExpect(MockMvcResultMatchers.jsonPath("$.creator.name").value("player1")).andExpect(MockMvcResultMatchers.jsonPath("$.winners.[0].name").value("player2")).andExpect(MockMvcResultMatchers.jsonPath("$.finished").value(true));
    }

    @Test
    @Order(3)
    public void testNewGame() throws Exception {
        authResponse = authService.login(createLoginDto());

        GameDto gameDto = new GameDto();
        gameDto.setPlayers(List.of(1L, 2L));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/jokenpo/game/new").header("Authorization", authResponse.getTokenType() + " " + authResponse.getAccessToken()).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(gameDto))).andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    @Order(3)
    public void testInsertPlayerMove() throws Exception {
        authResponse = authService.login(createLoginDto());

        GameMoveDto gameMoveDto = new GameMoveDto();
        gameMoveDto.setGameId(1L);
        gameMoveDto.setMoveId(2L);

        ResultDto resultDto = new ResultDto();
        resultDto.setMessage("Jogada realizada!");

        when(gameService.insertPlayerMove(ArgumentMatchers.any())).thenReturn(resultDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/jokenpo/game/move").header("Authorization", authResponse.getTokenType() + " " + authResponse.getAccessToken()).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(gameMoveDto))).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Jogada realizada!"));
    }

    @Test
    @Order(3)
    public void testFindGames() throws Exception {
        authResponse = authService.login(createLoginDto());

        // Criando uma lista de jogos de exemplo para o retorno do método findGames()
        List<Game> games = new ArrayList<>();
        Game game1 = createGame(1L, "player1", "player2", true);
        Game game2 = createGame(2L, "player3", "player4", false);
        games.add(game1);
        games.add(game2);

        // Definindo o comportamento esperado para o método findGames()
        when(gameService.findGames()).thenReturn(games);

        // Realizando a requisição GET para o endpoint "/api/v1/jokenpo/game"
        mockMvc.perform(get("/api/v1/jokenpo/game").header("Authorization", authResponse.getTokenType() + " " + authResponse.getAccessToken())).andExpect(status().isOk()).andExpect(jsonPath("$", notNullValue())).andExpect(jsonPath("$", hasSize(2)));
    }

    private Game createGame(Long id, String player1Name, String player2Name, boolean finished) {
        Game game = new Game();
        game.setId(id);
        game.setCreator(createPlayer(player1Name, 1L));
        game.setFinished(finished);
        game.setCreatedAt(LocalDateTime.now());
        Set<Player> winners = new HashSet<>();
        winners.add(createPlayer(player2Name, 2L));
        game.setWinners(winners);
        return game;
    }

    private Player createPlayer(String name, Long id) {
        Player player = new Player();
        player.setName(name);
        player.setId(id);
        return player;
    }

    private LoginDto createLoginDto() {
        LoginDto loginDto = new LoginDto();
        loginDto.setUsername("johny");
        loginDto.setPassword("12345");
        return loginDto;
    }
}