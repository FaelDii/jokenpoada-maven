package tech.ada.games.jokenpo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import tech.ada.games.jokenpo.dto.LoginDto;
import tech.ada.games.jokenpo.dto.PlayerDto;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestPropertySource(locations = "classpath:application-tests.properties")
public class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Order(1)
    public void shouldCreatePlayer() throws Exception {
        PlayerDto playerDto = new PlayerDto();
        playerDto.setName("David Santana");
        playerDto.setUsername("david");
        playerDto.setPassword("1234@A");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = writer.writeValueAsString(playerDto);

        this.mockMvc.perform(
                post("/api/v1/jokenpo/player/create")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    @Order(2)
    public void testLoginWithSucess() throws Exception {

        LoginDto loginDto = new LoginDto();
        loginDto.setUsername("david");
        loginDto.setPassword("1234@A");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = writer.writeValueAsString(loginDto);

        this.mockMvc.perform(
                post("/api/v1/jokenpo/login")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    @Order(2)
    public void testLoginWithWrongPassword() throws Exception {

        LoginDto loginDto = new LoginDto();
        loginDto.setUsername("david");
        loginDto.setPassword("12345");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = writer.writeValueAsString(loginDto);

        this.mockMvc.perform(
                post("/api/v1/jokenpo/login")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().isUnauthorized());

    }

    @Test
    @Order(2)
    public void testLoginWithoutPassword() throws Exception {

        LoginDto loginDto = new LoginDto();
        loginDto.setUsername("david");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = writer.writeValueAsString(loginDto);

        this.mockMvc.perform(
                        post("/api/v1/jokenpo/login")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .content(requestJson))
                .andDo(print())
                .andExpect(status().isUnauthorized());

    }

    @Test
    @Order(2)
    public void testLoginWithoutCredentials() throws Exception {


        this.mockMvc.perform(
                        post("/api/v1/jokenpo/login")
                                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isBadRequest());

    }
}
