package tech.ada.games.jokenpo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import tech.ada.games.jokenpo.dto.LoginDto;
import tech.ada.games.jokenpo.dto.MoveDto;
import tech.ada.games.jokenpo.dto.PlayerDto;
import tech.ada.games.jokenpo.model.Move;
import tech.ada.games.jokenpo.model.PlayerMove;
import tech.ada.games.jokenpo.response.AuthResponse;
import tech.ada.games.jokenpo.service.AuthService;
import tech.ada.games.jokenpo.service.GameService;
import tech.ada.games.jokenpo.service.MoveService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestPropertySource(locations="classpath:application-tests.properties")
public class MoveControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AuthService authService;
    @MockBean
    private MoveService moveService;

    @Autowired
    private ObjectMapper objectMapper;

    private AuthResponse authResponse;


    @Test
    @Order(1)
    public void shouldCreatePlayer() throws Exception {
        PlayerDto playerDto = new PlayerDto();
        playerDto.setName("Lucas Leme");
        playerDto.setUsername("lucas");
        playerDto.setPassword("1234");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(playerDto);
        this.mockMvc.perform(
                        post("/api/v1/jokenpo/player/create")
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .content(requestJson))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    @Order(1)
    void createMove() throws Exception{
        authResponse = authService.login(createLoginDto());

        List<PlayerMove> moves = new ArrayList<>();
        Move move = new Move(1L, "move2", moves);
        MoveDto moveDto = new MoveDto(move);
        moveDto.setMove("move");
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/jokenpo/move")
                .header("Authorization", authResponse.getTokenType() + " " + authResponse.getAccessToken()).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(moveDto)))
                .andExpect(MockMvcResultMatchers.status().isCreated());


    }

    @Test
    @Order(2)
    void findMoves() throws Exception {
        authResponse = authService.login(createLoginDto());
        this.mockMvc.perform(
                        get("/api/v1/jokenpo/move", "move")
                                .header("Authorization",authResponse.getTokenType()+" "+authResponse.getAccessToken()))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Order(2)
    void findMove() throws Exception {

        authResponse = authService.login(createLoginDto());

        this.mockMvc.perform(
                        get("/api/v1/jokenpo/move/{move}","faeldi")
                                .header("Authorization",authResponse.getTokenType()+" "+authResponse.getAccessToken()))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private LoginDto createLoginDto() {
        LoginDto loginDto = new LoginDto();
        loginDto.setUsername("lucas");
        loginDto.setPassword("1234");
        return loginDto;
    }
}