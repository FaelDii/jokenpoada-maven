package tech.ada.games.jokenpo.security;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SecurityUtilsTest {

    @Test
    @DisplayName("Obter login de usuário atual com sucesso via UserDetails")
    void testGetCurrentUserLogin_UserDetails() {
        UserDetails userDetails = mock(UserDetails.class);
        when(userDetails.getUsername()).thenReturn("usuarioLogado");
        Authentication authentication = Mockito.mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(userDetails);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        
        SecurityContextHolder.setContext(securityContext);
        
        String username = SecurityUtils.getCurrentUserLogin();
        
        assertEquals( "usuarioLogado", username);
    }

    @Test
    @DisplayName("Obter login de usuário atual com sucesso")
    void testGetCurrentUserLogin_String() {
        String expectedUsername = "usuarioLogado";
        Authentication authentication = Mockito.mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(expectedUsername);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);

        SecurityContextHolder.setContext(securityContext);

        String username = SecurityUtils.getCurrentUserLogin();

        assertEquals( "usuarioLogado", username);
    }

    @Test
    @DisplayName("Tentar obter login de usuário mas authentication é nulo")
    void testGetCurrentUserLogin_whenAuthenticationIsNull() {
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(null);

        SecurityContextHolder.setContext(securityContext);

        String username = SecurityUtils.getCurrentUserLogin();

        assertNull( username);
    }

}