package tech.ada.games.jokenpo.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class MoveTest {

    @Test
    public void testMove() {
        PlayerMove playerMove = new PlayerMove();
        Move move = new Move();
        move.setId(1L);
        move.setMove("move");
        String toStringMove = "Move(id=1, move=move, moves=null)";
        Assertions.assertEquals(toStringMove, move.toString());
    }

    @Test
    public void testMoveAllArgsConstructor() {
        Game game = new Game();
        Role role = new Role();
        role.setName("ROLE_USER");
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        Player player = new Player(1L,"player1","12345","player1Name",roles);
        List<PlayerMove> moves = new ArrayList<>();
        Move move = new Move(1L, "move", moves);
        PlayerMove playerMove = new PlayerMove(1L, game, player, move );

        String toStringMove = "Move(id=1, move=move, moves=[])";
        Assertions.assertEquals(toStringMove, move.toString());
    }
}