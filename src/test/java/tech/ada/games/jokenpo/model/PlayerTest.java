package tech.ada.games.jokenpo.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

public class PlayerTest {
    @Test
    public void testPlayer() {
        Role role = new Role();
        role.setName("ROLE_USER");
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        Player player = new Player(1L,"player1","12345","player1Name",roles);
        String toStringPlayer = "Player(id=1, username=player1, password=12345, name=player1Name, roles=[Role(id=null, name=ROLE_USER)])";
        Assertions.assertEquals(player.toString(),toStringPlayer);

    }
}