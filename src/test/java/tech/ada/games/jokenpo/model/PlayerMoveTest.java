package tech.ada.games.jokenpo.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import tech.ada.games.jokenpo.dto.MoveDto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PlayerMoveTest {
    private  Game game;
    private Move move;
    private  Player player;
    private PlayerMove playerMove;
    private Role role;

    @BeforeEach
    public void setUp() {
        game = new Game();
        role = new Role();
        role.setName("ROLE_USER");
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        player = new Player(1L,"player1","12345","player1Name",roles);
        List<PlayerMove> moves = new ArrayList<>();
        move = new Move(1L, "move", moves);
        playerMove = new PlayerMove(1L, game, player, move );
    }

    @Test
    public void testPlayerMove() {
        String stringTest = "PlayerMove(id=1, game=Game(id=null, creator=null, finished=null, createdAt=null, winners=null, players=null), player=Player(id=1, username=player1, password=12345, name=player1Name, roles=[Role(id=null, name=ROLE_USER)]), move=Move(id=1, move=move, moves=[]))";
        Assertions.assertEquals(stringTest, playerMove.toString());
    }

    @Test
    public void testID(){
        playerMove.setId(2L);
        Assertions.assertEquals(2L, playerMove.getId());
    }
    @Test
    public void testGame(){
        game.setId(1L);
        playerMove.setGame(game);
        String testString = "Game(id=1, creator=null, finished=null, createdAt=null, winners=null, players=null)";

        Assertions.assertEquals(testString, playerMove.getGame().toString());
    }




}