package tech.ada.games.jokenpo.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GameDtoTest {

    private GameDto gameDto;
    private List<Long> players;

    @BeforeEach
    void setUp() {
        players = Arrays.asList(1L, 2L);
        gameDto = new GameDto(players);
    }

    @Test
    void testNoArgsConstructor() {
        GameDto emptyGameDto = new GameDto();
        assertNull(emptyGameDto.getPlayers());
    }

    @Test
    void testAllArgsConstructor() {
        assertEquals(players, gameDto.getPlayers());
    }

    @Test
    void testBuilder() {
        GameDto gameDtoBuilt = GameDto.builder().players(players).build();
        assertEquals(players, gameDtoBuilt.getPlayers());
    }

    @Test
    void testGetPlayers() {
        assertEquals(players, gameDto.getPlayers());
    }

    @Test
    void testSetPlayers() {
        List<Long> newPlayers = Arrays.asList(3L, 4L);
        gameDto.setPlayers(newPlayers);
        assertEquals(newPlayers, gameDto.getPlayers());
    }
}