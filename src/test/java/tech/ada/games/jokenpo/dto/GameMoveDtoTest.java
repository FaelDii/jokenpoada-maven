package tech.ada.games.jokenpo.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameMoveDtoTest {

    private GameMoveDto gameMoveDto;
    private Long gameId;
    private Long moveId;

    @BeforeEach
    void setUp() {
        gameId = 1L;
        moveId = 2L;
        gameMoveDto = new GameMoveDto(gameId, moveId);
    }

    @Test
    void testNoArgsConstructor() {
        GameMoveDto emptyGameMoveDto = new GameMoveDto();
        assertNull(emptyGameMoveDto.getGameId());
        assertNull(emptyGameMoveDto.getMoveId());
    }

    @Test
    void testAllArgsConstructor() {
        assertEquals(gameId, gameMoveDto.getGameId());
        assertEquals(moveId, gameMoveDto.getMoveId());
    }

    @Test
    void testBuilder() {
        GameMoveDto gameMoveDtoBuilt = GameMoveDto.builder()
                .gameId(gameId)
                .moveId(moveId)
                .build();
        assertEquals(gameId, gameMoveDtoBuilt.getGameId());
        assertEquals(moveId, gameMoveDtoBuilt.getMoveId());
    }

    @Test
    void testGetGameId() {
        assertEquals(gameId, gameMoveDto.getGameId());
    }

    @Test
    void testSetGameId() {
        Long newGameId = 3L;
        gameMoveDto.setGameId(newGameId);
        assertEquals(newGameId, gameMoveDto.getGameId());
    }

    @Test
    void testGetMoveId() {
        assertEquals(moveId, gameMoveDto.getMoveId());
    }

    @Test
    void testSetMoveId() {
        Long newMoveId = 4L;
        gameMoveDto.setMoveId(newMoveId);
        assertEquals(newMoveId, gameMoveDto.getMoveId());
    }
}
