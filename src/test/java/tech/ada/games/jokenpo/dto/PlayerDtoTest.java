package tech.ada.games.jokenpo.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tech.ada.games.jokenpo.model.Player;


class PlayerDtoTest {

    @Test
    public void testPlayerDto() {
        PlayerDto playerDto1 = PlayerDto.builder().username("player1").password("123456").name("player1Name").build();
        PlayerDto playerDto2 = new PlayerDto("player1","123456","player1Name");
        Player player = new Player(1L,playerDto2.getUsername(),playerDto2.getPassword(), playerDto2.getName(),null);
        PlayerDto playerDto3 = new PlayerDto(player);
        Assertions.assertEquals(playerDto1.getUsername(),playerDto2.getUsername());
        Assertions.assertEquals(playerDto1.getPassword(),playerDto2.getPassword());
        Assertions.assertEquals(playerDto1.getName(),playerDto2.getName());
        Assertions.assertEquals(playerDto3.getUsername(),playerDto2.getUsername());
        Assertions.assertEquals(playerDto3.getPassword(),playerDto2.getPassword());
        Assertions.assertEquals(playerDto3.getName(),playerDto2.getName());
    }
}