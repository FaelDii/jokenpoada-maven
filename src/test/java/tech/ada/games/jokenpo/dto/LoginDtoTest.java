package tech.ada.games.jokenpo.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LoginDtoTest {

	private LoginDto loginDto;
	
	
    @BeforeEach
    void setUp() {
    
    	this.loginDto = new LoginDto();
    	this.loginDto.setUsername("david");
    	this.loginDto.setPassword("1234@A");

    }
    
    @Test
    void testNoArgsConstructor() {

    	LoginDto emptyLoginDto = new LoginDto();
    	
    	assertNull(emptyLoginDto.getUsername());
    	assertNull(emptyLoginDto.getPassword());
    	
    }
    
    @Test
    void testGetUsername() {
    	
    	assertEquals("david", this.loginDto.getUsername());
    	
    }
    
    @Test
    void testSetUsername() {
    	
    	this.loginDto.setUsername("davidSantana");
    	assertEquals("davidSantana", this.loginDto.getUsername());
    	
    }
    
    @Test
    void testGetPassword() {
    	
    	assertEquals("1234@A", this.loginDto.getPassword());
    	
    }
    
    @Test
    void testSetPassword() {
    	this.loginDto.setPassword("A1234@");
    	assertEquals("A1234@", this.loginDto.getPassword());

    }
}
