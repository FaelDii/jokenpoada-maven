package tech.ada.games.jokenpo.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tech.ada.games.jokenpo.model.Move;
import tech.ada.games.jokenpo.model.PlayerMove;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MoveDtoTest {

    private MoveDto moveDto;

    @BeforeEach
    void setUp() {
        moveDto = new MoveDto();
        moveDto.setMove("start");
    }

    @Test
    void setMove() {
        moveDto.setMove("move");
        Assertions.assertEquals("move", moveDto.getMove());
    }

    @Test
    void getMove() {
        Assertions.assertEquals("start", moveDto.getMove()
        );
    }

    @Test
    void initMove(){
        PlayerDto playerDto1 = PlayerDto.builder().username("player1").password("123456").name("player1Name").build();
        Move move = new Move();
        MoveDto moveDto2 = new MoveDto("move-dto");
    }

    @Test
    void testMoveDtoBuilder() {
        MoveDto moveDto1 = MoveDto.builder().move("move-builder").build();
        Assertions.assertNotNull(moveDto1);
        Assertions.assertEquals("move-builder", moveDto1.getMove());
    }

    @Test
    void testMoveDtoContructor(){
        List<PlayerMove> moves = new ArrayList<>();
        Move move = new Move(1L, "move2", moves);
        MoveDto moveDto2 = new MoveDto(move);
        Assertions.assertEquals("MoveDto(move=move2)", moveDto2.toString());

    }
}