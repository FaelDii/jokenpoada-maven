Feature: Criar usuario
  Cria um novo usuário para o sistema

  Scenario: Criacao de novo usuario sem passar os dados
    Given que um usuario quer jogar e nao esta cadastrado
    When  clica em criar usuario e nao fornece nenhum dado
    Then retorna erro e nao cria o usuario
