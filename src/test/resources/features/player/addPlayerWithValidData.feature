Feature: Criar usuario
  Cria um novo usuário para o sistema

  Scenario: Criacao de novo usuario com dados validos
    Given que um usuario quer jogar e ele nao esta cadastrado
    When  clica em criar usuario fornecendo name, username e password
    Then seu usuario e criado com os dados passados
